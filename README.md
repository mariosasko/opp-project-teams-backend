# OPP project teams - backend

Demo aplikacija Spring Boot backenda za potrebe predmeta OPP,
na gostujećem predavanju firme [CROZ](http://croz.net).

Verzija koda koju smo napravili na predavanju 25.10.2018. je tagirana s [`predavanje`](https://gitlab.com/hrvojesimic/opp-project-teams-backend/tree/predavanje).

Prezentacije se mogu skinuti s Google Drivea i otvoriti s Microsoft Powerpointom.
Nemojte ih gledati u pregledniku jer tamo neće raditi dobro.

- [prvi dio](https://drive.google.com/file/d/1jKW3By8EJ7UEmnv5mAgFI4iFUbC4sSQ3/view?usp=sharing)
- [drugi dio](https://drive.google.com/file/d/1f6j0FBOdprXxNXgtIK71u3KI8dimyF_R/view?usp=sharing)

Nakon te verzije dodano je [još dosta koda](https://gitlab.com/hrvojesimic/opp-project-teams-backend/commit/f643619c4340542b584a3edaace52872bc654a04)
koji upotpunjuje planiranu funkcionalnost. Konkretno:

 - dohvat studenta i grupe po ID-u
 - ažuriranje studenta metodom PUT
 - brisanje studenta metodom DELETE
 - izmjena imena grupe metodom PATCH
 - dodatne provjere pri kreiranju grupe
 - dodavanje i micanje članova iz grupe posebnim korištenjem metoda PUT i DELETE
 - ograničenje na veličinu grupe
 - inicijalizacija baze s nekoliko testnih studenata
 - primjeri testova

Probleme s projektom ili pitanja možete unijeti kao [issue](https://gitlab.com/hrvojesimic/opp-project-teams-backend/issues).
