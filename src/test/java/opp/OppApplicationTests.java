package opp;

import opp.domain.Group;
import opp.domain.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * Example functional test (calling REST service and backed up with in-memory database).
 * Depends on preloaded test data.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class OppApplicationTests {
	@LocalServerPort
	private int port;

	public static final Pattern GROUP_URI = Pattern.compile("/groups/(\\d+)");

	private String url(String path) {
		return "http://localhost:" + port + path;
	}

	@Test
	public void testPreloadedStudents() throws Exception {
    TestRestTemplate restTemplate = new TestRestTemplate();
		List<Student> list = restTemplate.getForObject(url("/students"), List.class);
		assertThat(list.size()).isEqualTo(8); // expects 8 test student
		assertThat(list.get(0).isLead()).isTrue();
		assertThat(list.get(0).getJmbag()).isEqualTo("1000000000");
		assertThat(list.get(1).isLead()).isTrue();
		assertThat(list.get(2).isLead()).isFalse(); // expects opp.test.student.leads: 2
	}

	@Test
	public void createGroupAndCheckoutResult() {
    TestRestTemplate restTemplate = new TestRestTemplate("1000000000", "pass");
    Map<String, String> nameMap = new HashMap<>();
		nameMap.put("name", "MyGroup");
    URI resultUri = restTemplate.postForLocation(url("/groups"), nameMap);
    String path = resultUri.getPath();
    assertThat(path).matches(GROUP_URI);
    ResponseEntity<Group> getGroupResponse = restTemplate.getForEntity(url(path), Group.class);
    assertThat(getGroupResponse.getStatusCodeValue()).isEqualTo(200);
    Group group = getGroupResponse.getBody();
    assertThat(group.getName()).isEqualTo("MyGroup");
    assertThat(group.getCoordinator().getJmbag()).isEqualTo("1000000000");
    assertThat(group.getMembers().size()).isEqualTo(1);
  }
}
