package opp.rest;

import opp.domain.Student;
import opp.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
  @Autowired
  private StudentService studentService;

  @GetMapping("")
  public List<Student> listStudents() {
    return studentService.listAll();
  }

  @GetMapping("/{id}")
  public Student getStudent(@PathVariable("id") long id) {
    return studentService.fetch(id);
  }

  @PostMapping("")
  @Secured("ROLE_ADMIN")
  public ResponseEntity<Student> createStudent(@RequestBody Student student) {
    Student saved = studentService.createStudent(student);
    return ResponseEntity.created(URI.create("/students/" + saved.getId())).body(saved);
  }

  @PutMapping("/{id}")
  @Secured("ROLE_ADMIN")
  public Student updateStudent(@PathVariable("id") Long id, @RequestBody Student student) {
    if (!student.getId().equals(id))
      throw new IllegalArgumentException("Student ID must be preserved");
    return studentService.updateStudent(student);
  }

  @DeleteMapping("/{id}")
  @Secured("ROLE_ADMIN")
  public Student deleteStudent(@PathVariable("id") long studentId) {
    return studentService.deleteStudent(studentId);
  }
}
