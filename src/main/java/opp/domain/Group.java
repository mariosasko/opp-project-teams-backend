package opp.domain;

import org.springframework.util.Assert;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Group of students. Consists of one student coordinator and several other members.
 * (List of all members of a group includes the coordinator.)
 * Uniquely identified by internal system ID (a Long), or a name.
 * @author Hrvoje Šimić hsimic@croz.net
 */
@Entity(name="SGROUP") // Note: "group" is reserved keyword in SQL
public class Group {

  @Id @GeneratedValue
  private Long id;

  @Column(unique=true, nullable=false)
  @Size(min=1, max=20)
  private String name;

  @OneToOne
  private Student coordinator;

  @OneToMany
  private Set<Student> members;

  public Group() {
  }

  /**
   * Sets name, coordinator, and members set with coordinator.
   * @param name group name
   * @param coordinator coordinator student
   * @throws IllegalArgumentException if any argument null,
   * or name lacking non-whitespace character, or with length over 20,
   */
  public Group(String name, Student coordinator) {
    Assert.hasText(name, "Group name must have text");
    Assert.isTrue(name.length() <= 20,
      "Group name max length is 20 characters, got: " + name);
    Assert.notNull(coordinator, "Coordinator must be set");
    this.name = name;
    this.coordinator = coordinator;
    this.members = new HashSet<>(Arrays.asList(coordinator));
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Student getCoordinator() {
    return coordinator;
  }

  public void setCoordinator(Student leader) {
    this.coordinator = leader;
  }

  public Set<Student> getMembers() {
    return members;
  }

  public void setMembers(Set<Student> members) {
    this.members = members;
  }

  @Override
  public String toString() {
    return "Group #" + id + " '" + name + "', coordinator=" + coordinator + ", members=" + members;
  }
}
