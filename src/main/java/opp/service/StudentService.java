package opp.service;

import opp.domain.Student;

import java.util.List;
import java.util.Optional;

/**
 * Manages student database.
 * @see Student
 * @author Hrvoje Šimić hsimic@croz.net
 */
public interface StudentService {
  /**
   * Lists all students in the system.
   * @return a list with all students
   */
  List<Student> listAll();

  /**
   * Fetches student with given ID.
   * @param studentId given student ID
   * @return student associated with given ID in the system
   * @throws EntityMissingException if student with that ID is not found
   * @see StudentService#findById(long)
   */
  Student fetch(long studentId);
  // Note: verb "fetch" in method name is typically used when identified object is expected

  /**
   * Creates new student in the system.
   * @param student object to create, with ID set to null
   * @return created student object in the system with ID set
   * @throws IllegalArgumentException if given student is null, or its ID is NOT null,
   * or its JMBAG is null or invalid
   * @throws RequestDeniedException if student with that JMBAG already exists in the system
   * @see Student
   */
  Student createStudent(Student student);

  /**
   * Finds student with given ID, if exists.
   * @param studentId given student ID
   * @return Optional with value of student associated with given ID in the system,
   * or no value if one does not exist
   * @see StudentService#fetch
   */
  Optional<Student> findById(long studentId);

  /**
   * Finds the student with given JMBAG.
   * @param jmbag student JMBAG
   * @return Optional with value of a student associated with given JMBAG exists in the system,
   * no value otherwise
   * @throws IllegalArgumentException if given JMBAG is null
   */
  Optional<Student> findByJmbag(String jmbag);

  /**
   * Updates the student with that same ID.
   * @param student object to update, with ID set
   * @return updated student object in the system
   * @throws IllegalArgumentException if given object is null, has null ID, or has null or invalid JMBAG
   * @throws EntityMissingException if student with given ID is not found
   * @throws RequestDeniedException if another student with some other ID and the same JMBAG already exists
   * @see StudentService#createStudent(Student)
   */
  Student updateStudent(Student student);

  /**
   * Deletes one student.
   * @param studentId ID of student to delete from the system
   * @return deleted data
   * @throws EntityMissingException if student with that ID is not found
   */
  Student deleteStudent(long studentId);
}
